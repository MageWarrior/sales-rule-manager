<?php
/**
 * MageWarrior Sales Rule Manager Extension
 *
 * This Magento extension allows store owners to easily view and reset a
 * customer's sales rule usage from within their Customer Edit page in the Admin
 * panel.
 *
 * Have a problem with the extension? File a report on our
 * {@link https://bitbucket.org/MageWarrior/sales-rule-manager/issues Issue Tracker}!
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0) that is
 * bundled with this package in the file "LICENSE". It is also available through
 * the World Wide Web at this URL:
 *     http://opensource.org/licenses/osl-3.0.php
 *
 * If you did not receive a copy of the license and are unable to obtain it
 * through the World Wide Web, please send an e-mail to license@magewarrior.us
 * so that we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Sales Rule Manager to
 * newer versions in the future. If you wish to customize Sales Rule Manager for
 * your needs please refer to
 * https://bitbucket.org/MageWarrior/sales-rule-manager/ for more information.
 *
 * @category  MageWarrior
 * @package   MageWarrior_SalesRuleManager
 * @copyright Copyright (c) 2014 MageWarrior E-commerce Development (http://magewarrior.us/)
 * @license   http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 * @link      https://bitbucket.org/MageWarrior/sales-rule-manager/ Bitbucket Source Repository
 */

/**
 * Event Observer Model
 *
 * @category MageWarrior
 * @package  MageWarrior_SalesRuleManager
 * @author   MageWarrior Development Team <devteam@magewarrior.us>
 */
class MageWarrior_SalesRuleManager_Model_Observer
{
    /**
     * Adds a new tab to the Customer Edit page in the Admin panel
     *
     * Event: core_block_abstract_prepare_layout_after
     *
     * @param Varien_Event_Observer $observer
     */
    public function addTabToCustomerAdmin(Varien_Event_Observer $observer)
    {
        $block = $observer->getEvent()->getBlock();
        $request = Mage::app()->getRequest();

        if ($block instanceof Mage_Adminhtml_Block_Customer_Edit_Tabs
            && ($request->getActionName() === 'edit' || $request->getParam('type'))
            && Mage::registry('current_customer')->getId()
        ) {
            $block->addTabAfter(
                'promohistory',
                array(
                    'label' => Mage::helper('salesrulemanager')->__('Promotion History'),
                    'class' => 'ajax',
                    'url'   => $this->getUrl('*/*/promohistory', array('_current' => true)),
                ),
                'orders'
            );
        }
    }
}
